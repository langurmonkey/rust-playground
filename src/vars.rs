// Variables are immutable by default unless specified otherwise
// Rust is a bock-scoped language
pub fn run() {
    // Mutability
    let name = "Toni";
    let mut age = 37;
    println!("My name is {} and I'm {}", name, age);
    age = 38;
    println!("Now my age is {}", age);

    // Define constant
    const ID: i64 = 001;
    println!("ID: {}", ID);

    // Multiple assignment
    let (my_name, my_age) = ("Toni", 37);
    println!("My name is {} and I'm {}", my_name, my_age);

}
