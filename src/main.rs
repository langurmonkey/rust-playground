mod arrays;
mod cli;
mod enums;
mod guess;
mod pointers;
mod print;
mod strings;
mod structs;
mod tuples;
mod types;
mod vars;
mod vectors;

fn main() {
    print::run();
    vars::run();
    types::run();
    strings::run();
    tuples::run();
    arrays::run();
    vectors::run();
    pointers::run();
    structs::run();
    enums::run();
    cli::run();
    //guess::guess_game(0, 10);
}
