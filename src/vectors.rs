// Vectors are resizable arrays
use std::mem;

pub fn run() {
    let mut numbers: Vec<i64> = vec![1, 2, 3, 4, 5];

    println!("{:?}", numbers);

    // Single value
    println!("Single value: {}", numbers[2]);

    // Assign
    numbers[2] = 34;
    println!("Single value (mut): {}", numbers[2]);

    // Get length
    println!("Vector length: {}", numbers.len());

    // Arrays are stacked allocated
    println!("Size: {} bytes", mem::size_of_val(&numbers));

    // Slices
    let slice: &[i64] = &numbers[0..2];
    println!("Slice: {:?}", slice);
    println!("Direct slice: {:?}", &numbers[0..2]);

    // Add elements
    numbers.push(234);
    numbers.push(564);
    println!("Push element: {:?}", numbers);

    // Pop
    numbers.pop();
    println!("Pop element: {:?}", numbers);

    // Loop
    for x in numbers.iter() {
        println!("{}", x);
    }

    // Loop & mutate
    for x in numbers.iter_mut() {
        *x *= 2;
    }
    println!("{:?}", numbers);
}
