use rand::Rng;
use text_io::read;

// Dead-simple guessing game
pub fn guess_game(low: u16, high: u16) {
    let num: u16 = rand::thread_rng().gen_range(low, high);
    println!("Guess a number between {} and {}:", low, high);

    let mut guess: u16 = read!();
    while num != guess {
        if num < guess {
            println!("My number is smaller than your guess (↓), guess again:");
        } else {
            println!("My number is larger than your guess (↑), guess again:");
        }
        guess = read!();
    }
    println!("Correct! The number was {}", num);
    return;
}
