// Structs are custom data types

// Traditional struct
struct Color {
    r: u8,
    g: u8,
    b: u8,
}

// Tuple struct
struct Col(u8, u8, u8);

// Struct with functions
struct Person {
    name: String,
    lastname: String,
}

impl Person {
    // Construct
    fn new(name: &str, lastname: &str) -> Person {
        Person {
            name: name.to_string(),
            lastname: lastname.to_string(),
        }
    }

    fn full_name(&self) -> String {
        format!("{} {}", self.name, self.lastname)
    }

    fn set_name(&mut self, newname: &str) {
        self.name = newname.to_string();
    }

    fn to_tuple(self) -> (String, String) {
        (self.name, self.lastname)
    }
}

pub fn run() {
    // TRADITIONAL
    let mut c = Color { r: 255, g: 0, b: 0 };
    println!("Color: {} {} {}", c.r, c.g, c.b);

    c.g = 127;
    println!("Color: {} {} {}", c.r, c.g, c.b);
    println!("Size: {} bytes", std::mem::size_of_val(&c));

    // TUPLE
    let mut cl = Col(255, 0, 0);
    println!("Color: {} {} {}", cl.0, cl.1, cl.2);

    cl.1 = 127;
    println!("Color: {} {} {}", cl.0, cl.1, cl.2);
    println!("Size: {} bytes", std::mem::size_of_val(&cl));

    // Person
    let mut p: Person = Person::new("John", "Arbuckle");
    println!("Person: {} {}", p.name, p.lastname);

    p.name = String::from("Garfield");
    println!("Person: {} {}", p.name, p.lastname);
    println!("Full name: {}", p.full_name());

    p.set_name("Fucker");
    println!("Full name: {}", p.full_name());

    println!("{:?}", p.to_tuple());
}
