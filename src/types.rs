/*
The primitive types of Rust
Integers: u8, i8, u16, i16, u32, i32, u64, i64, u128, i128 (number of bits they take in memory)
Floats: f32, f64
Boolean (bool)
Characters (char)
Tuples
Arrays
*/
pub fn run() {
    // Default is i32
    let x = 1;
    // Default is f64
    let y = 2.1;

    // Explicit
    let z: f32 = 1.1;
    let w: i64 = 23452423423432;

    // Max size
    println!("Max i32: {}", std::i32::MAX);
    println!("Max i64: {}", std::i64::MAX);
    println!("Max f32: {}", std::f32::MAX);
    println!("Max f64: {}", std::f64::MAX);

    // Boolean
    let is_active: bool = true;

    // Boolean from expression
    let is_greater = 10 > x;

    // Character (unicode)
    let a1 = 'a';
    let face = '\u{1F600}';

    println!("{:?}", (x, y, z, w, is_active, is_greater, a1, face));
}
