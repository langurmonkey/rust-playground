pub fn run() {
    // Print to console
    println!();
    println!("PRINT EXAMPLES in print.rs file");

    // Basic formatting
    println!("{} is from {}", "Marty McFly", "Hill Valley");

    // Positional arguments
    println!(
        "{0} is from {1} and {0} likes to {2}",
        "Marty McFly", "Hill Valley", "play the guitar"
    );

    // Named arguments
    println!(
        "{name} likes to play {activity}",
        name = "Lea",
        activity = "UNO"
    );

    // Placeholder traits
    println!("Bin: {:b}, Hex: {:x}, Octal: {:o}", 10, 10, 10);

    // Placeholder debug trait
    println!("See here: {:?}", ("Job", 12, false));

    // Basic math
    println!("2 + 2 = {}", 2 + 2);
}
