// Arrays - fixed list where elements are the same types
use std::mem;

pub fn run() {
    let mut numbers: [i64; 5] = [1, 2, 3, 4, 5];

    println!("{:?}", numbers);

    // Single value
    println!("Single value: {}", numbers[2]);

    // Assign
    numbers[2] = 34;
    println!("Single value (mut): {}", numbers[2]);

    // Get length
    println!("Length: {}", numbers.len());

    // Arrays are stacked allocated
    println!("Size: {} bytes", mem::size_of_val(&numbers));

    // Slices
    let slice: &[i64] = &numbers[0..2];
    println!("Slice: {:?}", slice);
    println!("Direct slice: {:?}", &numbers[0..2]);
}
