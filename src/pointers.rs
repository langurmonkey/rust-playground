pub fn run() {
    // Primitive array
    let arr1 = [1, 2, 3];
    let arr2 = arr1;

    println!("Values: {:?}", (arr1, arr2));

    // With non-primitives, if you assign another variable to a piece of data, the first one
    // will no longer hold that value.
    let vec1: Vec<i32> = vec![1, 2, 3];
    let vec2: &Vec<i32> = &vec1;

    println!("Values: {:?}", (&vec1, vec2));
}
