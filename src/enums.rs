// Enums are types which have a few definite values

enum Status {
    // Variants
    Idle,
    Queued,
    Loading,
    Ready,
}

fn check_status(s: Status) {
    // Perform action depending on status
    match s {
        Status::Idle => println!("We are idle"),
        Status::Queued => println!("We are queued"),
        Status::Loading => println!("We are loading"),
        Status::Ready => println!("We are ready"),
    }
}

pub fn run() {
    let system1 = Status::Idle;
    let system2 = Status::Queued;
    let system3 = Status::Loading;
    let system4 = Status::Ready;

    check_status(system1);
    check_status(system2);
    check_status(system3);
    check_status(system4);
}
