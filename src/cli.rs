use std::env;

pub fn run() {
    let args: Vec<String> = env::args().collect();

    println!("Args: {:?}", args);

    println!("Executable: {}", args[0]);
    if args.len() > 1 {
        let cmd = &args[1];
        println!("Command: {}", cmd);
        match cmd.as_str() {
            "help" => println!("Print help"),
            "version" => println!("Version 1.0"),
            _ => println!("Not recognized"),
        }
    } else {
        println!("No command");
    }
}
